public class GameLauncher {

    // initializing main method
    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
